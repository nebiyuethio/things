# Use the official Node.js image as base
FROM node:alpine

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./
#COPY ./prisma prisma

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

RUN npx prisma generate

# Expose the port the app runs on
EXPOSE 3000

# Environment variables
ENV DATABASE_URL=${DATABASE_URL}

# Start the Next.js app
CMD ["npm", "run", "dev"]
