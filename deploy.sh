#!/bin/bash

# Navigate to the Terraform directory
cd terraform

# Initialize Terraform
terraform init

# Apply the Terraform scripts
terraform apply -auto-approve

# Navigate to the Docker directory
cd ../docker

# Deploy the Docker stack
docker stack deploy -c docker-compose.yml myapp