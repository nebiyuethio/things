variable "do_token" {
  description = "DigitalOcean Token"
  type        = string
}
variable "ssh_private_key_path" {
  description = "ssh key"
  type        = string
}