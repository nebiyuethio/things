terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.10.0"  # Replace with the desired version
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "ssh_key" {
  name       = "my ssh"
  public_key = file("${var.ssh_private_key_path}.pub")
}

resource "digitalocean_droplet" "web" {
  image  = "docker-20-04"
  name   = "web-1"
  region = "nyc1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.ssh_key.fingerprint]
}

output "droplet_ip" {
  value = digitalocean_droplet.web.ipv4_address
}